public class Test {
    public static void main(String[] args) {
        System.out.println("qwe".hashCode());
        System.out.println("qwe".hashCode());
        System.out.println("asd".hashCode());

        class Student {
            int year;
            String name = "";

            Student(int year, String name) {
                this.year = year;
                this.name = name;
            }

            @Override
            public int hashCode() {
                return year + name.hashCode();
            }
        }

        Student s1 = new Student(2001, "Ivan");
        Student s2 = new Student(2002, "Eugene");
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
    }
}
